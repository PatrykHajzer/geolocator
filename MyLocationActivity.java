package com.example.patryk.geolocator;

import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.SphericalUtil;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MyLocationActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    LatLng position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_location);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        position = getIntent().getExtras().getParcelable("Pos");

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng wfia = new LatLng(51.116360, 17.028000);
        double distance = SphericalUtil.computeDistanceBetween(position, wfia);

        LatLng olawa = new LatLng(50.9466, 17.2926);
        LatLng lodz = new LatLng(51.759445, 19.457216);
        LatLng opole = new LatLng(50.667728, 17.928600);


        // Add a marker
        mMap.addMarker(new MarkerOptions().position(position).title("Marker in your position"));
        mMap.addMarker(new MarkerOptions().position(wfia).title(String.valueOf(distance/1000) + " " + "km"));
        Polyline line = mMap.addPolyline(new PolylineOptions()
                .add(position, opole)
                .add(opole, lodz)
                .add(lodz, olawa)
                .add(olawa, wfia)
                .width(5)
                .color(Color.RED));


        mMap.moveCamera(CameraUpdateFactory.newLatLng(position));
    }

}
