package com.example.patryk.geolocator;

import android.graphics.Color;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.SphericalUtil;


public class RownikActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_road);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng first = new LatLng(0.000000, 0.000000);
        LatLng secound = new LatLng(0.000000, 180.000000);


        // Add a marker in Sydney and move the camera
        mMap.addMarker(new MarkerOptions().position(first).title("first"));
        mMap.addMarker(new MarkerOptions().position(secound).title("secound"));
        double equatorDistance = (SphericalUtil.computeDistanceBetween(first, secound) * 2) / 1000;
        String info = String.valueOf(equatorDistance) + " " + "km";





        mMap.getUiSettings().setZoomControlsEnabled(true);



        mMap.moveCamera(CameraUpdateFactory.newLatLng(first));

        Toast.makeText(this, info, Toast.LENGTH_LONG).show();
    }
}
