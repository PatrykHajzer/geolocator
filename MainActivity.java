package com.example.patryk.geolocator;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.IOException;
import java.util.List;

public class MainActivity extends AppCompatActivity {


//    //pobieranie lokalizacji z gps AD1
//    public Button locationBtn;
//    public TextView showLocationTxt;
//    private FusedLocationProviderClient mFusedLocationClient;
    //
    //Pobieranie adresu i pokazywanie go na mapie
    public EditText placeAdress;
    public Button showAdress;

    //bubttony do drogi
    public Button showRoad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //AD1
//        locationBtn = (Button) findViewById(R.id.getLocationBtn);
//        showLocationTxt = (TextView) findViewById(R.id.showLocationTxt);
//        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        //AD2
        placeAdress = (EditText) findViewById(R.id.placeAdress);
        showAdress = (Button) findViewById(R.id.showAdressBtn);
        //AD3
        showRoad = (Button) findViewById(R.id.showRoadBtn);

//        locationBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                getLocation();
//
//            }
//        });

        showAdress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LatLng latLng = new LatLng(0,0);
                if (placeAdress != null){
                    latLng = getLocationFromAddress(MainActivity.this, placeAdress.getText().toString());
                } else {

//                    getLocation();

                }
                Intent i = new Intent(MainActivity.this, MyLocationActivity.class);
                i.putExtra("Pos", latLng);
                startActivity(i);
            }
        });

        showRoad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(MainActivity.this, RownikActivity.class);

                startActivity(i);

            }
        });


    }
    public LatLng getLocationFromAddress(Context context, String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;

        try {
            // May throw an IOException
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }

            Address location = address.get(0);
            p1 = new LatLng(location.getLatitude(), location.getLongitude() );

        } catch (IOException ex) {

            ex.printStackTrace();
        }

        return p1;
    }







    //AD1
    public static String locationStringFromLocation(final Location location) {
        return Location.convert(location.getLatitude(), Location.FORMAT_DEGREES) + " " + Location.convert(location.getLongitude(), Location.FORMAT_DEGREES);
    }
    public static LatLng locationToLatLang(final Location location){
        return new LatLng(location.getLatitude(), location.getLongitude());
    }

//    public void getLocation() {
//
//        if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
//        }else{
//            mFusedLocationClient.getLastLocation()
//                    .addOnSuccessListener(this, new OnSuccessListener<Location>() {
//
//                        @Override
//                        public void onSuccess(Location location) {
//                            // Got last known location. In some rare situations this can be null.
//                            if (location != null) {
////                                latLng = locationToLatLang(location);
//                                showLocationTxt.setText(locationStringFromLocation(location));
//                            }
//                        }
//                    });
//        }
//
//
//    }
//    //AD1
}

